defmodule Solution do
  def nb_year(p0, percent, aug, p, n \\ 0) do
    if p0 < p do
      case trunc(p0 * (1 + 0.01 * percent)) + aug do
        new_p0 when new_p0 < p -> nb_year(new_p0, percent, aug, p, n + 1)
        _ -> n + 1
      end
    else
      n
    end
  end
end